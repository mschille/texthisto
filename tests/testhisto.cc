/** @file tests/testhisto.cc
 *
 * @brief a little test that uses much of the functionality
 *
 * not a proper unit test as such, but better than nothing...
 *
 * @author Manuel Schiller <Manuel.Schiller@cern.ch>
 * @date 2018-03-17
 */
#include <ctime>
#include <random>
#include <iostream>

#include "Histo1D.h"
#include "TextCanvas.h"

int main()
{
    std::mt19937 rng(std::time(nullptr));
    std::normal_distribution<double> gauss;

    Histo1D<double, double> h("foo;x;y", 100, -5. ,5.);
    for (unsigned i = 0; i < 10000; ++i) {
        h.Fill(gauss(rng));
    }

    TextCanvas c;
    c.Draw(h);

    return 0;
}

// vim: sw=4:tw=78:et:ft=cpp
