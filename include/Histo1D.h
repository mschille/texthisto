/** @file Histo1D.h
 *
 * @brief one dimensional histogram class
 *
 * @author Manuel Schiller <Manuel.Schiller@glasgow.ac.uk>
 * @date 2018-03-17
 */
#pragma once

#include <cmath>
#include <vector>
#include <algorithm>
#include <stdexcept>
#include <type_traits>

/** @brief one dimensional histogram class
 *
 * A primitive 1D histogram class that offers both fixed size and variable
 * sized bins. Users of ROOT should feel right at home, although some of the
 * more horrible interface choices of ROOT have been ignored.
 *
 * Bins are numbered starting from 0, where 0 is the underflow bin, then
 * follow a number of regular bins (1, ..., N), followed by the overflow bin
 * N+1.
 *
 * The class keeps running averages of mean and variance, and exposes mean and
 * RMS to the user. It also keeps track of the number of entries, the
 * integral (sum of weights), and the sum of absolute values of weights.
 *
 * The data types for quantities on the X and Y axis are templated, so users
 * can choose, but default to double.
 *
 * @tparam TY   type for the y axis
 * @tparam TX   type for the x axis
 *
 * Here's a little example to fill (and draw) such a histogram:
 *
 * @code
 * #include <iostream>
 * #include <random>
 * #include <ctime>
 *
 * #include "Histo1D.h"
 * #include "Histo1DPrinter.h"
 *
 * std::mt19937 rng(std::time(nullptr));
 * std::normal_distribution<double> gauss;
 *
 * // create a histogram with 100 bins from -5 to 5
 * Histo1D<double, double> h("foo;x;y", 100, -5. ,5.);
 *
 * // fill with 10000 random numbers drawn from a normal distribution
 * for (unsigned i = 0; i < 10000; ++i) {
 *     h.Fill(gauss(rng));
 * }
 *
 * // draw the thing on std::cout
 * Histo1DPrinter hp;
 * hp.Draw(h);
 * @endcode
 *
 * @author Manuel Schiller <Manuel.Schiller@glasgow.ac.uk>
 * @date 2018-03-17
 */
template <typename TY = double, typename TX = double>
class Histo1D {
    protected:
        std::vector<TX> m_buckets;
        std::vector<TY> m_bins;
        std::vector<TY> m_bins2;
        TX m_min;
        TX m_max;
        std::size_t m_nentries;
        TY m_integral;
        TY m_absintegral;
        TX m_mean;
        TX m_var;
        std::string m_title;

        // check that the template arguments make sense...
        static_assert(std::is_arithmetic<TX>::value,
                "TX must be arithmetic type");
        static_assert(std::is_arithmetic<TY>::value,
                "TY must be arithmetic type");

        /// fill the bins of weight^2
        void fillBins2()
        {
            m_bins2.reserve(m_bins.size());
            for (const auto& el: m_bins) {
                m_bins2.emplace_back(el * el);
            }
        }

    public:
        /// copy constructor
        Histo1D(const Histo1D<TY, TX>&) = default;
        /// move constructor
        Histo1D(Histo1D<TY, TX>&&) = default;
        /// (copy) assignment operator
        Histo1D& operator=(const Histo1D<TY, TX>&) = default;
        /// (move) assignment operator
        Histo1D& operator=(Histo1D<TY, TX>&&) = default;

        /** @brief construct a 1D histogram with uniform bin size
         *
         * @param       title histogram title (with axis labels separated by
         *              semicolons, i.e. "title;x [xunit];y [yunit]" will set
         *              both x and y axis labels in addition to the title)
         * @param nbins number of bins
         * @param min   lower edge of first bin
         * @param max   upper edge of last bin
         *
         * @code
         * Histo1D<float, float> h("title;x label;y label", 100, -5., 5.);
         * h.Fill(0.42);
         * @endcode
         */
        Histo1D(const std::string& title, std::size_t nbins, TX min, TX max) :
            m_bins(2 + nbins, TY(0)), m_min(min), m_max(max), m_nentries(0),
            m_integral(0), m_absintegral(0), m_mean(0), m_var(0), m_title(title)
        {}

        /** @brief construct a 1D histogram with user defined bin sizes
         *
         * @tparam IT   type of iterator used to pass the bin bounds
         *
         * @param       title histogram title (with axis labels separated by
         *              semicolons, i.e. "title;x [xunit];y [yunit]" will set
         *              both x and y axis labels in addition to the title)
         * @param first iterator pointing to first bin bound
         * @param last  iterator pointing one past the last bin bound
         *
         * @code
         * double bounds[] = { 0., 0.1, 0.3, 0.9, 1.5 };
         * Histo1D<float, float> h("title;x label;y label", &bounds[0], &bounds[5]);
         * h.Fill(0.42);
         * @endcode
         */
        template <typename IT>
        Histo1D(const std::string& title, IT first,
                typename std::enable_if<std::is_same<TX,
                typename std::remove_cv<decltype(*std::declval<IT>())
                >::type>::value, IT>::type last) :
            m_buckets(first, last), m_bins(2 + m_buckets.size(), TY(0)),
            m_min(m_buckets.front()), m_max(m_buckets.back()), m_title(title)
        {}

        /** @brief return the number of bins on x axis
         *
         * @returns number of bins on x axis
         */
        std::size_t NbinsX() const noexcept { return m_bins.size() - 2; }

        /** @brief return number of entries
         *
         * @returns number of entries
         */
        std::size_t GetNEntries() const noexcept { return m_nentries; }

        /** @brief return integral of the histogram
         *
         * @returns integral of the histogram (sum of weights)
         */
        TY GetIntegral() const noexcept { return m_integral; }

        /** @brief return absolute value of integral of the histogram
         *
         * @returns absolute value integral of the histogram (sum of abs(weights))
         */
        TY GetAbsIntegral() const noexcept { return m_absintegral; }

        /** @brief return mean of the histograma
         *
         * @returns number mean of the histogram
         */
        TX GetMean() const noexcept { return m_mean; }

        /** @brief return RMS of the histogram
         *
         * @returns number RMS of the histogram
         */
        TX GetRMS() const noexcept{ return std::sqrt(m_var); }

        /** @brief return title of the histogram
         *
         * @returns title of the histogram
         *
         * @note this returns the string that was passed to the constructor,
         * i.e. it may still contain axis labels separated by semicolons
         */
        const std::string& GetTitle() const noexcept { return m_title; }

        /** @brief set the number of entries
         *
         * @param entries       number of entries to set
         */
        void SetNEntries(std::size_t entries) noexcept { m_nentries = entries; }

        /** @brief set integral
         *
         * @param integral       integral to set
         */
        void SetIntegral(TY integral) noexcept
        { m_integral = integral, m_absintegral = std::abs(integral); }

        /** @brief set absolute value of integral
         *
         * @param absintegral       absolute value of integral to set
         */
        void SetAbsIntegral(TY absintegral) noexcept
        { m_absintegral = absintegral; }

        /** @brief set mean
         *
         * @param mean       mean to set
         */
        void SetMean(TX mean) noexcept { m_mean = mean; }

        /** @brief set RMS
         *
         * @param rms       RMS to set
         */
        void SetRMS(TX rms) noexcept { m_var = rms * rms; }

        /** @brief find bin number corresponding to x
         *
         * With bin bounds arranged like @f$x_0 < x_1 < ... < x_N <
         * x_{N+1}@f$, find bin i such that @f$x_i \le x < x_{i + 1}X@f$.
         * Bin 0 is the underflow bin, and bin NbinsX() + 1 is the overflow
         * bin.
         *
         * @returns bin number between 0 (underflow) and NbinsX() + 1
         * (overflow) inclusive.
         */
        std::size_t FindBin(TX x) const noexcept
        {
            if (m_min <= x && x <= m_max) {
                if (m_buckets.empty()) {
                    return 1 + (m_bins.size() - 2) * (x - m_min) / (m_max - m_min);
                } else {
                    return 1 + std::distance(m_buckets.begin(),
                            std::lower_bound(m_buckets.begin(), m_buckets.end(), x));
                }
            } else if (x < m_min) {
                return 0;
            } else {
                return m_bins.size() - 1;
            }
        }

        /** @brief get the low edge of a bin
         *
         * @param bin   bin for which to get the low edge
         *
         * @return low edge of a bin
         */
        TX GetBinLoEdge(std::size_t bin) const
        {
            if (1 <= bin && bin <= NbinsX() + 1) {
                const auto f = double(bin - 1) / double(NbinsX());
                return m_min * (1. - f) + m_max * f;
            } else if (0 == bin) {
                return -std::numeric_limits<TX>::min();
            }
            throw std::out_of_range("Histo1D");
        }

        /** @brief get the high edge of a bin
         *
         * @param bin   bin for which to get the high edge
         *
         * @return high edge of a bin
         */
        TX GetBinHiEdge(std::size_t bin) const
        {
            if (0 <= bin && bin <= NbinsX()) {
                const auto f = double(bin) / double(NbinsX());
                return m_min * (1. - f) + m_max * f;
            } else if (NbinsX() + 1 == bin) {
                return std::numeric_limits<TX>::min();
            }
            throw std::out_of_range("Histo1D");
        }

        /** @brief get the centre of a bin
         *
         * @param bin   bin for which to get the centre
         *
         * @return centre of a bin
         */
        TX GetBinCenter(std::size_t bin) const
        {
            if (1 <= bin && bin <= NbinsX() + 1) {
                const auto f = double(2 * bin - 1) / double(2 * NbinsX());
                return m_min * (1. - f) + m_max * f;
            }
            throw std::out_of_range("Histo1D");
        }

        /** @brief get bin content
         *
         * @param bin   bin for which to get the content
         *
         * @return content of a bin
         */
        TY GetBinContent(std::size_t bin) const
        {
            if (bin < m_bins.size()) return m_bins[bin];
            throw std::out_of_range("Histo1D");
        }

        /** @brief get bin error
         *
         * @param bin   bin for which to get the error
         *
         * @return error of a bin
         */
        TY GetBinError(std::size_t bin) const
        {
            if (bin < m_bins.size()) {
                if (m_bins2.empty()) return m_bins[bin];
                else return std::sqrt(m_bins2[bin]);
            }
            throw std::out_of_range("Histo1D");
        }

        /** @brief set bin content
         *
         * @param bin           bin for which to set the content
         * @param content       bin content to set
         */
        void SetBinContent(std::size_t bin, TY content)
        {
            if (bin < m_bins.size()) {
                m_bins[bin] = content;
                return;
            }
            throw std::out_of_range("Histo1D");
        }

        /** @brief set bin error
         *
         * @param bin           bin for which to set the error
         * @param error         bin error to set
         */
        void SetBinError(std::size_t bin, TY error)
        {
            if (bin < m_bins.size()) {
                if (m_bins2.empty()) fillBins2();
                m_bins2[bin] = error * error;
                return;
            }
            throw std::out_of_range("Histo1D");
        }

        /** @brief set bin content and error
         *
         * @param bin           bin for which to set the content and error
         * @param content       bin content to set
         * @param error         bin error to set
         */
        void SetBinContent(std::size_t bin, TY content, TY error)
        {
            if (bin < m_bins.size()) {
                m_bins[bin] = content;
                if (m_bins2.empty()) fillBins2();
                m_bins2[bin] = error * error;
            }
            throw std::out_of_range("Histo1D");
        }

        /** @brief fill the histogram
         *
         * @param x     x value which to fill histogram with
         */
        void Fill(TX x) noexcept
        {
            const auto bin = FindBin(x);
            m_bins[bin] += TY(1);
            if (!m_bins2.empty()) {
                m_bins2[bin] += TY(1);
            }
            ++m_nentries;
            m_integral += TY(1);
            const auto oldintegral = m_absintegral;
            m_absintegral += TY(1);
            const auto oldmean = m_mean;
            m_mean *= oldintegral / m_absintegral;
            m_mean += x / m_absintegral;
            m_var *= oldintegral / m_absintegral;
            m_var += (x - oldmean) * (x - m_mean) / m_absintegral;
        }

        /** @brief fill the histogram
         *
         * @param x     x value which to fill histogram with
         * @param w     weight with which to fill in value at x
         */
        void Fill(TX x, TY w)
        {
            const auto bin = FindBin(x);
            m_bins[bin] += w;
            if (!m_bins2.empty()) {
                m_bins2[bin] += TY(1);
            } else if (TY(1) != w) {
                fillBins2();
                m_bins2[bin] += w * w;
            }
            ++m_nentries;
            m_integral += w;
            const auto oldintegral = m_absintegral;
            m_absintegral += std::abs(w);
            const auto oldmean = m_mean;
            m_mean *= oldintegral / m_absintegral;
            m_mean += std::abs(w) * x / m_absintegral;
            m_var *= oldintegral / m_absintegral;
            m_var += std::abs(w) * (x - oldmean) * (x - m_mean) / m_absintegral;
        }

        /** @brief get the minimum of the histogram
         *
         * @param withErrorBar  include error bars in calculation
         * @param withUnderflow include underflow bin in calculation
         * @param withOverflow  include overflow bin in calculation
         *
         * @returns minimum of the histogram
         */
        TY GetMinimum(bool withErrorBar = false, bool withUnderflow = false,
                bool withOverflow = false) const noexcept
        {
            TY min = std::numeric_limits<TY>::max();
            if (m_bins2.empty()) {
                for (std::size_t i = 1 - withUnderflow,
                        end = NbinsX() + withOverflow; i <= end; ++i) {
                    const auto y = m_bins[i] - withErrorBar * std::sqrt(m_bins[i]);
                    if (y < min) min = y;
                }
            } else {
                for (std::size_t i = 1 - withUnderflow,
                        end = NbinsX() + withOverflow; i <= end; ++i) {
                    const auto y = m_bins[i] - withErrorBar * std::sqrt(m_bins2[i]);
                    if (y < min) min = y;
                }
            }
            return min;
        }

        /** @brief get the maximum of the histogram
         *
         * @param withErrorBar  include error bars in calculation
         * @param withUnderflow include underflow bin in calculation
         * @param withOverflow  include overflow bin in calculation
         *
         * @returns maximum of the histogram
         */
        TY GetMaximum(bool withErrorBar = false, bool withUnderflow = false,
                bool withOverflow = false) const noexcept
        {
            TY max = std::numeric_limits<TY>::min();
            if (m_bins2.empty()) {
                for (std::size_t i = 1 - withUnderflow,
                        end = NbinsX() + withOverflow; i <= end; ++i) {
                    const auto y = m_bins[i] + withErrorBar * std::sqrt(m_bins[i]);
                    if (y > max) max = y;
                }
            } else {
                for (std::size_t i = 1 - withUnderflow,
                        end = NbinsX() + withOverflow; i <= end; ++i) {
                    const auto y = m_bins[i] + withErrorBar * std::sqrt(m_bins2[i]);
                    if (y > max) max = y;
                }
            }
            return max;
        }
};

// vim: sw=4:tw=78:et:ft=cpp
