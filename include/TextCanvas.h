/* @file include/TextCanvas.h
 *
 * @brief print histograms to a dumb terminal
 *
 * @author Manuel Schiller <Manuel.Schiller@cern.ch>
 * @date 2017-03-17
 */

#pragma once

#include <array>
#include <tuple>
#include <vector>
#include <string>
#include <iostream>
#include <functional>

// forward declarations
template <typename, typename> class Histo1D;
class TH1;

/** @brief print a histogram to a dumb terminal or file.
 *
 * Example: Supposing you wanted to dump an 80x25 character plot of a
 * histogram to a log file, you could use the following code:
 *
 * @code
 * #include <ofstream>
 * #include "TextCanvas.h"
 * #include "Histo1D.h"
 *
 * std::ofstream logfile("logfile.txt");
 * TextCanvas hp(80, 25, logfile);
 * Histo1D<double, double> histo = get_histo_from_somewhere();
 * hp.Draw(histo);
 * @endcode
 *
 * @author Manuel Schiller <Manuel.Schiller@cern.ch>
 * @date 2017-03-17
 */
class TextCanvas {
    public:
        enum StatsBoxPos {None, Left, Right};
    protected:
        using Screen = std::vector<std::string>;

        std::ostream& m_os;
        Screen m_screen;
        unsigned short m_dim[2];
        bool m_dirty;
        StatsBoxPos m_statsBoxPos = Right;

        // crack the title string into histogram, x, y axis titles at ';'
        static std::array<std::string, 3> crackTitle(const std::string& title);

        /// draw a set of axes
        void drawAxes(double xrange[2], double yrange[2]);
        /// draw titles (histogram, axes)
        void drawTitles(const std::string& title);
        /// draw the histogram itself
        void drawHisto(char marker, std::size_t nbins,
                double xrange[2], double yrange[2],
                std::function<double(std::size_t)> content,
                std::function<double(std::size_t)> hiEdge,
                std::function<std::tuple<std::size_t, double, double,
                double, double>()> stats);
        /// normalise screen content (remove trailing blanks)
        void normaliseScreen();
        /// draw the screen itself
        void drawScreen();
        /// draw stats box
        void drawStats(std::size_t entries, double mean, double rms,
                double uflo, double oflo);

        void adjustWidth(std::string& str, unsigned width);
        void putFlushLeft(std::string& str,
                unsigned x, unsigned y, unsigned width);
        void putFlushRight(std::string& str,
                unsigned x, unsigned y, unsigned width);
        void putCentered(std::string& str,
                unsigned x, unsigned y, unsigned width);

    public:
        /// constructor
        TextCanvas(unsigned short columns = -1, unsigned short lines = -1,
                std::ostream& os = std::cout);
        ~TextCanvas();

        /// clear the screen buffer
        void clear();
        /// create a new screen buffer in which to draw
        void resize(unsigned short x, unsigned short y);
        /// flush the screen buffer to the underlying ostream
        void flush();

        /** @brief draw a histogram
         *
         * FIXME: add more documentation...
         */
        template <typename TY, typename TX>
        void Draw(const Histo1D<TY, TX>& h);
        // planned...
        void Draw(const TH1& h);
        void Draw(const TH1* h) { Draw(*h); }

        StatsBoxPos getStatsBoxPos() const noexcept { return m_statsBoxPos; }
        void setStatsBoxPos(StatsBoxPos pos) noexcept { m_statsBoxPos = pos; }
};

template <typename TY, typename TX>
void TextCanvas::Draw(const Histo1D<TY, TX>& h)
{
    // work out histogram dimensions
    double xrange[2] = { h.GetBinLoEdge(1), h.GetBinHiEdge(h.NbinsX()) };
    double yrange[2] = { h.GetMinimum(), h.GetMaximum() };
    drawAxes(xrange, yrange);
    drawTitles(h.GetTitle());
    drawHisto('*', h.NbinsX(), xrange, yrange,
            // use std::function to type-erase the type of histogram used
            [&h] (std::size_t bin) { return h.GetBinContent(1 + bin); },
            [&h] (std::size_t bin) { return h.GetBinHiEdge(1 + bin); },
            [&h] () { return std::tuple<std::size_t, double, double,
            double, double>(h.GetNEntries(), h.GetMean(), h.GetRMS(),
            h.GetBinContent(0), h.GetBinContent(h.NbinsX() + 1)); });
}

// vim: sw=4:tw=78:et:ft=cpp
