# libtexthisto

libtexthisto is a standalone library for plotting 1D histograms to a dumb
terminal using ASCII art. Use is somewhat reminiscent of ROOT's TH1 histograms,
but the implementation is much leaner, and simpler.

Output will look something like this:

```text
y                                     foo
*10^+02
+4.240+-------------------------------*-*------------------------------+
      |                            *  ***             Entries     10000|
+3.794|                            * *  **            Mean    -0.007749|
      |                            * *  * *           RMS         1.003|
+3.347|                           * *     *           Underflow       0|
      |                          *        *           Overflow        0|
+2.901|                          *         *                           |
      |                         **         **                          |
+2.455|                         *           *                          |
      |                        *            **                         |
      |                        *              *                        |
+1.785|                       *                *                       |
      |                      *                 **                      |
+1.339|                     **                  *                      |
      |                    *                     *                     |
+0.893|                   **                      *                    |
      |                  *                        **                   |
+0.446|                 **                          *                  |
      |              ***                             ****              |
+0.000****************----------------------------------****************
   -5.000 -3.889 -2.778 -1.667 -0.556 +0.556 +1.667 +2.778 +3.889 +5.000
                                                               x *10^+00
```

The code to produce this plot is super-simple, and gives some indication as to
how the library can be used:

```c++
#include <ctime>
#include <random>
#include <iostream>

#include "Histo1D.h"
#include "TextCanvas.h"

int main()
{
    // pseudo random number generator plus normal distribution from C++
    // standard library
    std::mt19937 rng(std::time(nullptr));
    std::normal_distribution<double> gauss;

    // create and fill histogram
    Histo1D<double, double> h("foo;x;y", 100, -5. ,5.);
    for (unsigned i = 0; i < 10000; ++i) {
        h.Fill(gauss(rng));
    }

    // paint it on a text mode canvas
    TextCanvas c; // destructor will flush the canvas to std::cout
    c.Draw(h);

    return 0;
}
```
