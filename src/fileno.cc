/** @file fileno.cc
 *
 * @return return file descriptor underlying a C++ std::fstream or similar
 *
 * @author Richard B. Kreckl
 * @date 2005-04-02
 *      Code taken from https://www.ginac.de/~kreckel/fileno/. Original author
 *      put code into the public domain.
 *
 * @author Manuel Schiller <Manuel.Schiller@cern.ch>
 * @date 2018-03-17
 *      All mistakes and problems are probably due to me doing the
 *      copy-and-paste, and I've added support for libc++.
 */
#include <cstdio>  // declaration of ::fileno
#include <fstream>  // for basic_filebuf template
#include <cerrno>

#include "fileno.h"

#if defined(_LIBCPP_VERSION)
#include <typeinfo>
#include <__std_stream>
/** @brief namespace with tools to access to private member of a class
 *
 * Ordinarily, accessing or taking the address of a private member of a class
 * is forbidden. This rule is relaxed in template instantiations. This is used
 * to instantiate a template with a static member that holds a pointer to the
 * desired private member. To this end, one needs to define a tag, i.e. a type
 * that defines the type of the pointer to private member, and stands in for
 * the private member to which access is not allowed.
 *
 * Usage example:
 *
 * @code
 * class demonstrator {
 *     private:
 *         int member;
 *     public:
 *         demonstrator(int i) : member(i) {}
 * };
 *
 * // tag type
 * struct tag { using type = int demonstator::*; };
 * template class private_member::associate<tag, &demonstrator::member>;
 *
 * void fun()
 * {
 *     demonstrator d(42);
 *     std::cout << "d.member = " << d.*private_member::ptr<tag>::value <<
 *         std::endl;
 * }
 * @endcode
 *
 * @author Manuel Schiller <Manuel.Schiller@cern.ch>
 * @data 2018-03-26
 */
namespace private_member {
    /// a struct to hold the value associated with TAG
    template <typename TAG> struct ptr { static typename TAG::type value; };
    /// a memory location to hold the value associated with TAG
    template<typename TAG> typename TAG::type ptr<TAG>::value;

    /// a struct to save a value to ptr<TAG>::value
    template <typename TAG, typename TAG::type val> class associate {
        /// constructor saves to private_member_ptr<TAG>::value
        associate() { ptr<TAG>::value = val; };
        /// static instance causes
        static associate instance;
    };
    /// memory location for instances of save_value
    template <typename TAG, typename TAG::type val>
    associate<TAG, val> associate<TAG, val>::instance;
} // namespace private_member

template <typename charT, typename traits>
struct fb_tag { using type = FILE* std::basic_filebuf<charT, traits>::*; };

template class private_member::associate<
    fb_tag<char, std::char_traits<char> >,
    &std::basic_filebuf<char, std::char_traits<char> >::__file_>;
template class private_member::associate<
    fb_tag<wchar_t, std::char_traits<wchar_t> >,
    &std::basic_filebuf<wchar_t, std::char_traits<wchar_t> >::__file_>;

template <typename charT>
struct ib_tag { using type = FILE* std::__stdinbuf<charT>::*; };

template class private_member::associate<
    ib_tag<char>, &std::__1::__stdinbuf<char>::__file_>;
template class private_member::associate<
    ib_tag<wchar_t>, &std::__1::__stdinbuf<wchar_t>::__file_>;

template <typename charT>
struct ob_tag { using type = FILE* std::__stdoutbuf<charT>::*; };

template class private_member::associate<
    ob_tag<char>, &std::__1::__stdoutbuf<char>::__file_>;
template class private_member::associate<
    ob_tag<wchar_t>, &std::__1::__stdoutbuf<wchar_t>::__file_>;

#endif // defined(_LIBCPP_VERSION)

#if defined(__GLIBCXX__) || (defined(__GLIBCPP__) && __GLIBCPP__>=20020514)  // GCC >= 3.1.0
# include <ext/stdio_filebuf.h>
#endif
#if defined(__GLIBCXX__) // GCC >= 3.4.0
# include <ext/stdio_sync_filebuf.h>
#endif

//! Similar to fileno(3), but taking a C++ stream as argument instead of a
//! FILE*.  Note that there is no way for the library to track what you do with
//! the descriptor, so be careful.
//! \return  The integer file descriptor associated with the stream, or -1 if
//!   that stream is invalid.  In the latter case, for the sake of keeping the
//!   code as similar to fileno(3), errno is set to EBADF.
//! \see  The <A HREF="https://www.ginac.de/~kreckel/fileno/">upstream page at
//!   https://www.ginac.de/~kreckel/fileno/</A> of this code provides more
//!   detailed information.
template <typename charT, typename traits>
inline int
fileno_hack(const std::basic_ios<charT, traits>& stream)
{
    // Some C++ runtime libraries shipped with ancient GCC, Sun Pro,
    // Sun WS/Forte 5/6, Compaq C++ supported non-standard file descriptor
    // access basic_filebuf<>::fd().  Alas, starting from GCC 3.1, the GNU C++
    // runtime removes all non-standard std::filebuf methods and provides an
    // extension template class __gnu_cxx::stdio_filebuf on all systems where
    // that appears to make sense (i.e. at least all Unix systems).  Starting
    // from GCC 3.4, there is an __gnu_cxx::stdio_sync_filebuf, in addition.
    // Sorry, darling, I must get brutal to fetch the darn file descriptor!
    // Please complain to your compiler/libstdc++ vendor...
#if defined(__GLIBCXX__) || defined(__GLIBCPP__)
    // OK, stop reading here, because it's getting obscene.  Cross fingers!
# if defined(__GLIBCXX__)  // >= GCC 3.4.0
    // This applies to cin, cout and cerr when not synced with stdio:
    typedef __gnu_cxx::stdio_filebuf<charT, traits> unix_filebuf_t;
    unix_filebuf_t* fbuf = dynamic_cast<unix_filebuf_t*>(stream.rdbuf());
    if (fbuf != NULL) {
        return fbuf->fd();
    }

    // This applies to filestreams:
    typedef std::basic_filebuf<charT, traits> filebuf_t;
    filebuf_t* bbuf = dynamic_cast<filebuf_t*>(stream.rdbuf());
    if (bbuf != NULL) {
        // This subclass is only there for accessing the FILE*.  Ouuwww, sucks!
        struct my_filebuf : public std::basic_filebuf<charT, traits> {
            int fd() { return this->_M_file.fd(); }
        };
        return static_cast<my_filebuf*>(bbuf)->fd();
    }

    // This applies to cin, cout and cerr when synced with stdio:
    typedef __gnu_cxx::stdio_sync_filebuf<charT, traits> sync_filebuf_t;
    sync_filebuf_t* sbuf = dynamic_cast<sync_filebuf_t*>(stream.rdbuf());
    if (sbuf != NULL) {
#  if (__GLIBCXX__<20040906) // GCC < 3.4.2
        // This subclass is only there for accessing the FILE*.
        // See GCC PR#14600 and PR#16411.
        struct my_filebuf : public sync_filebuf_t {
            my_filebuf();  // Dummy ctor keeps the compiler happy.
            // Note: stdio_sync_filebuf has a FILE* as its first (but private)
            // member variable.  However, it is derived from basic_streambuf<>
            // and the FILE* is the first non-inherited member variable.
            FILE* c_file() {
                return *(FILE**)((char*)this + sizeof(std::basic_streambuf<charT, traits>));
            }
        };
        return ::fileno(static_cast<my_filebuf*>(sbuf)->c_file());
#  else
        return ::fileno(sbuf->file());
#  endif
    }
# else  // GCC < 3.4.0 used __GLIBCPP__
#  if (__GLIBCPP__>=20020514)  // GCC >= 3.1.0
    // This applies to cin, cout and cerr:
    typedef __gnu_cxx::stdio_filebuf<charT, traits> unix_filebuf_t;
    unix_filebuf_t* buf = dynamic_cast<unix_filebuf_t*>(stream.rdbuf());
    if (buf != NULL) {
        return buf->fd();
    }

    // This applies to filestreams:
    typedef std::basic_filebuf<charT, traits> filebuf_t;
    filebuf_t* bbuf = dynamic_cast<filebuf_t*>(stream.rdbuf());
    if (bbuf != NULL) {
        // This subclass is only there for accessing the FILE*.  Ouuwww, sucks!
        struct my_filebuf : public std::basic_filebuf<charT, traits> {
            // Note: _M_file is of type __basic_file<char> which has a
            // FILE* as its first (but private) member variable.
            FILE* c_file() { return *(FILE**)(&this->_M_file); }
        };
        FILE* c_file = static_cast<my_filebuf*>(bbuf)->c_file();
        if (c_file != NULL) {  // Could be NULL for failed ifstreams.
            return ::fileno(c_file);
        }
    }
#  else  // GCC 3.0.x
    typedef std::basic_filebuf<charT, traits> filebuf_t;
    filebuf_t* fbuf = dynamic_cast<filebuf_t*>(stream.rdbuf());
    if (fbuf != NULL) {
        struct my_filebuf : public filebuf_t {
            // Note: basic_filebuf<charT, traits> has a __basic_file<charT>* as
            // its first (but private) member variable.  Since it is derived
            // from basic_streambuf<charT, traits> we can guess its offset.
            // __basic_file<charT> in turn has a FILE* as its first (but
            // private) member variable.  Get it by brute force.  Oh, geez!
            FILE* c_file() {
                std::__basic_file<charT>* ptr_M_file = *(std::__basic_file<charT>**)((char*)this + sizeof(std::basic_streambuf<charT, traits>));
#  if _GLIBCPP_BASIC_FILE_INHERITANCE
                // __basic_file<charT> inherits from __basic_file_base<charT>
                return *(FILE**)((char*)ptr_M_file + sizeof(std::__basic_file_base<charT>));
#  else
                // __basic_file<charT> is base class, but with vptr.
                return *(FILE**)((char*)ptr_M_file + sizeof(void*));
#  endif
            }
        };
        return ::fileno(static_cast<my_filebuf*>(fbuf)->c_file());
    }
#  endif
# endif
#elif defined(_LIBCPP_VERSION)
    const std::basic_streambuf<charT, traits>* sbuf = stream.rdbuf();
    // for some really funny reason dynamic_casts to the various derived
    // streambuf types fail, so we have to make do with a bit of ugly RTTI to
    // identify what we're dealing with - basic_filebuf, __stdinbuf, or
    // __stdoutbuf...
    FILE* f = nullptr;
    if (0 == std::strcmp(typeid(*sbuf).name(),
                typeid(std::basic_filebuf<charT, traits>&).name())) {
        const std::basic_filebuf<charT, traits>* fbuf =
            static_cast<const std::basic_filebuf<charT, traits>*>(sbuf);
        f = fbuf->*private_member::ptr<fb_tag<charT, traits> >::value;
    } else if (0 == std::strcmp(typeid(*sbuf).name(),
                typeid(std::__1::__stdinbuf<charT>&).name())) {
        const std::__1::__stdinbuf<charT> *ibuf =
            static_cast<const std::__1::__stdinbuf<charT>*>(sbuf);
        f = ibuf->*private_member::ptr<ib_tag<charT> >::value;
    } else if (0 == std::strcmp(typeid(*sbuf).name(),
                typeid(std::__1::__stdoutbuf<charT>&).name())) {
        const std::__1::__stdoutbuf<charT> *obuf =
            static_cast<const std::__1::__stdoutbuf<charT>*>(sbuf);
        f = obuf->*private_member::ptr<ob_tag<charT> >::value;
    } else {
        // no idea what it is - fail
    } 
    if (f) {
        return fileno(f);
    }
#else // ends defined(_LIBCPP_VERSION)
#  error "Does anybody know how to fetch the bloody file descriptor?"
    return stream.rdbuf()->fd();  // Maybe a good start?
#endif
    errno = EBADF;
    return -1;
}

//! 8-Bit character instantiation: fileno(ios).
template <>
int
fileno<char>(const std::ios& stream)
{
    return fileno_hack(stream);
}

#if !(defined(__GLIBCXX__) || defined(__GLIBCPP__)) || (defined(_GLIBCPP_USE_WCHAR_T) || defined(_GLIBCXX_USE_WCHAR_T))
//! Wide character instantiation: fileno(wios).
template <>
int
fileno<wchar_t>(const std::wios& stream)
{
    return fileno_hack(stream);
}
#endif

// vim: sw=4:tw=78:et:ft=cpp
