/** @file fileno.h
 *
 * @return return file descriptor underlying a C++ std::fstream or similar
 *
 * Code taken from https://www.ginac.de/~kreckel/fileno/. Original author put
 * code into the public domain.
 *
 * @author Richard B. Kreckl
 * @date 2005-04-02
 *
 * All mistakes and problems are probably due to the person doing the
 * copy-and-paste:
 *
 * @author Manuel Schiller <Manuel.Schiller@cern.ch>
 * @date 2018-03-17
 */
#pragma once

#include <iosfwd>

template <typename charT, typename traits>
int fileno(const std::basic_ios<charT, traits>& stream);

// vim: sw=4:tw=78:et:ft=cpp
