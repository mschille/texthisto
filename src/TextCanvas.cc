/* @file src/TextCanvas.cc
 *
 * @brief print histograms to a dumb terminal
 *
 * @author Manuel Schiller <Manuel.Schiller@cern.ch>
 * @date 2017-03-17
 */

// C stuff
#include <cctype>
#include <cstdio>
#include <cassert>
#include <sys/ioctl.h>

// C++ standard library stuff
#include <array>
#include <tuple>
#include <vector>
#include <string>
#include <utility>
#include <iostream>
#include <algorithm>

// local headers
#include "Histo1D.h"
#include "TextCanvas.h"

#include "fileno.h"

TextCanvas::TextCanvas(unsigned short columns, unsigned short lines, std::ostream& os) :
    m_os(os), m_dim{columns, lines}, m_dirty(false)
{
    // -1 in columns or lines means autoselect, if possible
    if (-1 == short(columns) || -1 == short(lines)) {
        int fd;
        switch ((fd = fileno(os))) {
        default:
            {
                // fire off an ioctl to get window size
                struct winsize size;
                int err = ioctl(fd, TIOCGWINSZ, &size);
                if (-1 != err) {
                    // if successful, fill in the missing bits
                    if (-1 == short(columns)) m_dim[0] = size.ws_col;
                    if (-1 == short(lines)) m_dim[1] = size.ws_row - 1;
                    break;
                }
            } // fallthrough intended!
#if __cplusplus == 201703L
            [[fallthrough]];
#endif
        case -1:
            // unable to get fd, or ioctl failed - guess something that most
            // terminals will be able to display...
            if (-1 == short(columns)) m_dim[0] = 80;
            if (-1 == short(lines)) m_dim[1] = 24;
        }
    }
    assert(40 <= m_dim[0] && 15 <= m_dim[1]);
    clear();
}

TextCanvas::~TextCanvas()
{
    flush();
}

void TextCanvas::clear()
{
    m_screen.clear();
    m_screen.resize(m_dim[1], std::string(m_dim[0], ' '));
    m_dirty = false;
}

void TextCanvas::resize(unsigned short x, unsigned short y)
{
    m_dim[0] = x, m_dim[1] = y;
    clear();
}

void TextCanvas::flush()
{
    if (m_dirty) {
        normaliseScreen();
        drawScreen();
        clear();
    }
}

// start with some helpers which help us put strings where they belong
void TextCanvas::adjustWidth(std::string& str, unsigned width)
{
    if (str.size() > width) {
        str[width - 3] = str[width - 2] = str[width - 1] = '.';
        str.erase(str.begin() + width, str.end());
    }
}

void TextCanvas::putFlushLeft(std::string& str,
        unsigned x, unsigned y, unsigned width)
{
    adjustWidth(str, width);
    std::copy(str.begin(), str.end(), m_screen[y].begin() + x);
}

void TextCanvas::putFlushRight(std::string& str,
        unsigned x, unsigned y, unsigned width)
{
    adjustWidth(str, width);
    std::copy_backward(str.begin(), str.end(),
            m_screen[y].begin() + x + 1);
}

void TextCanvas::putCentered(std::string& str,
        unsigned x, unsigned y, unsigned width)
{
    adjustWidth(str, width);
    std::copy(str.begin(), str.end(),
            m_screen[y].begin() + x - str.size() / 2);
}

void TextCanvas::drawAxes(double xrange[2], double yrange[2])
{
    m_dirty = true;
    // we aim for a screen layout like this:
    // +----------------------------------------+
    // |y axis title                            |
    // |*10^+YY          title                  |
    // |y axis+--------------------------------+|
    // |labels|                                ||
    // |   .  |                                ||
    // |   .  |                                ||
    // |   .  |                                ||
    // |      +--------------------------------+|
    // |      x axis labels...                  |
    // |               x axis title      *10^+XX|
    // +----------------------------------------+
    //
    // additional remark: snprintf zero-terminates strings, so for bits where
    // we do not overwrite that zero termination in the screen buffer, we need
    // to write a blank ourselves...
    //
    // step 0: work out exponents for scientific notiation of axis labels, and
    // print them
    const int xpow10 = std::floor(std::log10(std::max(
                    std::abs(xrange[1]), std::abs(xrange[0]))));
    const auto xscale = std::pow(10., xpow10);
    const int ypow10 = std::floor(std::log10(std::max(
                    std::abs(yrange[1]), std::abs(yrange[0]))));
    const auto yscale = std::pow(10., ypow10);
    std::snprintf(&m_screen[m_dim[1] - 1][m_dim[0] - 7], 8, "*10^%+03d", xpow10);
    std::snprintf(&m_screen[1][0], 8, "*10^%+03d", ypow10);
    if (7 < m_dim[0]) m_screen[1][7] = ' ';
    // step 1: write y axis labels
    for (unsigned divy: { 10, 5, 3, 2 }) { // either 10, 5, 3, 2 subdivisions
        if ((m_dim[1] - 3) / (divy - 1) < 1) continue;
        const double idiv = 1. / (divy - 1);
        for (unsigned i = 0; i <= (divy - 1); ++i) {
            const unsigned short y = std::round(2 + (m_dim[1] - 5) * idiv * i);
            assert(0 <= y && y <= m_dim[1]);
            const double f = (y - 2) / double(m_dim[1] - 5);
            const double yy = (1. - f) * (yrange[1] / yscale) +
                f * (yrange[0] / yscale);
            std::snprintf(&m_screen[y][0], 7, "%+-6.3f", yy);
        }
        break;
    }
    // step 2: write x axis labels
    for (unsigned divx: { 10, 5, 3, 2 }) { // either 10, 5, 3, 2 subdivisions
        if ((m_dim[0] - 3) / (7 * (divx - 1)) < 1) continue;
        const double idiv = 1. / (divx - 1);
        for (unsigned i = 0; i <= (divx - 1); ++i) {
            const unsigned short x = 3 + std::round(((m_dim[0] - 9) * idiv * i));
            assert(0 <= x && x <= m_dim[0]);
            const double f = (x - 3) / double(m_dim[0] - 9);
            const double xx = (1. - f) * (xrange[0] / xscale) +
                f * (xrange[1] / xscale);
            std::snprintf(&m_screen[m_dim[1] - 2][x], 7, "%+-6.3f", xx);
            if (x + 6 < m_dim[0]) m_screen[m_dim[1] - 2][x + 6] = ' ';
        }
        break;
    }
    // step 3: four corners...
    m_screen[           2][           6] = '+';
    m_screen[           2][m_dim[0] - 1] = '+';
    m_screen[m_dim[1] - 3][           6] = '+';
    m_screen[m_dim[1] - 3][m_dim[0] - 1] = '+';
    // step 5: lines connecting the corners
    for (unsigned short x = 7; x < m_dim[0] - 1; ++x) {
        m_screen[           2][x] = '-';
        m_screen[m_dim[1] - 3][x] = '-';
    }
    for (unsigned short y = 3; y < m_dim[1] - 3; ++y) {
        m_screen[y][           6] = '|';
        m_screen[y][m_dim[0] - 1] = '|';
    }
}

std::array<std::string, 3> TextCanvas::crackTitle(const std::string& title)
{
    // find up to three substrings, separated by semicolons
    std::array<std::size_t, 4> ranges = {
        0, std::string::npos, std::string::npos, std::string::npos
    };
    ranges[1] = title.find(";", ranges[0]);
    ranges[2] = title.find(";", ranges[1] + 1);
    for (unsigned i = 1; i < 4; ++i) {
        if (std::string::npos == ranges[i]) {
            ranges[i] = title.size();
        }
    }
    // start stripping off whitespace on both ends
    const auto stripSpace = [&title] (std::size_t first, std::size_t last) {
        if (first > last) return std::make_pair(last, last);
        while (first != last) {
            if (std::isspace(title[first])) {
                ++first;
                continue;
            }
            if (std::isspace(title[last - 1])) {
                --last;
                continue;
            }
            break;
        }
        return std::make_pair(first, last);
    };
    std::array<std::size_t, 6> finalranges;
    std::tie(finalranges[0], finalranges[1]) = stripSpace(ranges[0], ranges[1]);
    std::tie(finalranges[2], finalranges[3]) = stripSpace(ranges[1] + 1, ranges[2]);
    std::tie(finalranges[4], finalranges[5]) = stripSpace(ranges[2] + 1, ranges[3]);
    return {{
            title.substr(finalranges[0], finalranges[1] - finalranges[0]),
            title.substr(finalranges[2], finalranges[3] - finalranges[2]),
            title.substr(finalranges[4], finalranges[5] - finalranges[4]) }};
}

void TextCanvas::drawTitles(const std::string& title)
{
    m_dirty = true;
    // hack apart the title string so we have histogram, x and y axis titles
    // separately
    auto titles = crackTitle(title);
    putCentered(titles[0], 6 + (m_dim[0] - 6) / 2, 0, m_dim[0] - 6);
    putFlushLeft(titles[2], 0, 0, 6 + (m_dim[0] - 6) / 2 - titles[0].size() / 2);
    putFlushRight(titles[1], m_dim[0] - 9, m_dim[1] - 1, m_dim[0] - 8);
}

void TextCanvas::drawStats(std::size_t entries, double mean, double rms,
        double uflo, double oflo)
{
    if (Right == m_statsBoxPos) {
        char buf[24];
        std::snprintf(&buf[0], 24, "Entries %9lu", entries);
        std::string sbuf(buf);
        putFlushRight(sbuf, m_dim[0] - 2, 3, 20);
        std::snprintf(&buf[0], 24, "Mean %12.4g", mean);
        sbuf = &buf[0];
        putFlushRight(sbuf, m_dim[0] - 2, 4, 20);
        std::snprintf(&buf[0], 24, "RMS  %12.4g", rms);
        sbuf = &buf[0];
        putFlushRight(sbuf, m_dim[0] - 2, 5, 20);
        std::snprintf(&buf[0], 24, "Underflow %7g", uflo);
        sbuf = &buf[0];
        putFlushRight(sbuf, m_dim[0] - 2, 6, 20);
        std::snprintf(&buf[0], 24, "Overflow  %7g", oflo);
        sbuf = &buf[0];
        putFlushRight(sbuf, m_dim[0] - 2, 7, 20);
    } else if (Left == m_statsBoxPos) {
        char buf[24];
        std::snprintf(&buf[0], 24, "Entries %9lu", entries);
        std::string sbuf(buf);
        putFlushLeft(sbuf, 7, 3, 20);
        std::snprintf(&buf[0], 24, "Mean %12.4g", mean);
        sbuf = &buf[0];
        putFlushLeft(sbuf, 7, 4, 20);
        std::snprintf(&buf[0], 24, "RMS  %12.4g", rms);
        sbuf = &buf[0];
        putFlushLeft(sbuf, 7, 5, 20);
        std::snprintf(&buf[0], 24, "Underflow %7g", uflo);
        sbuf = &buf[0];
        putFlushLeft(sbuf, 7, 6, 20);
        std::snprintf(&buf[0], 24, "Overflow  %7g", oflo);
        sbuf = &buf[0];
        putFlushLeft(sbuf, 7, 7, 20);
    }
}

void TextCanvas::drawHisto(char marker, std::size_t nbins,
        double xrange[2], double yrange[2],
        std::function<double(std::size_t)> content,
        std::function<double(std::size_t)> hiEdge,
        std::function<std::tuple<std::size_t, double, double,
            double, double>() > stats)
{
    m_dirty = true;
    std::size_t bin = 0;
    auto cxmax = xrange[0];
    unsigned yyminlast = 0, yymaxlast = m_dim[1] - 5;
    for (unsigned xx = 0; xx < unsigned(m_dim[0] - 6); ++xx) {
        const auto fxmin = xx / double(m_dim[0] - 6);
        const auto fxmax = (xx + 1) / double(m_dim[0] - 6);
        assert(0. <= fxmin && fxmin < fxmax && fxmax <= 1.);
        const auto xmin = (1. - fxmin) * xrange[0] + fxmin * xrange[1];
        const auto xmax = (1. - fxmax) * xrange[0] + fxmax * xrange[1];
        assert(xrange[0] <= xmin && xmin < xmax && xmax <= xrange[1]);
        // skip bins that have no overlap
        while (cxmax <= xmin) {
            cxmax = hiEdge(bin);
            ++bin;
            assert(xrange[0] <= cxmax && cxmax <= xrange[1]);
            assert(bin <= nbins);
        }
        // work out which bins contribute
        auto ymin = content(bin);
        auto ymax = ymin;
        while (cxmax <= xmax && bin <= nbins) {
            const auto c = content(bin);
            if (c < ymin) ymin = c;
            if (ymax < c) ymax = c;
            cxmax = hiEdge(bin);
            ++bin;
            assert(bin <= nbins + 1);
            assert(xrange[0] <= cxmax && cxmax <= xrange[1]);
        }
        // translate to screen coordinates
        unsigned yymin = std::round((m_dim[1] - 5) *
                (ymin / (yrange[1] - yrange[0]) - yrange[0] /
                 (yrange[1] - yrange[0])));
        unsigned yymax = std::round((m_dim[1] - 5) *
                (ymax / (yrange[1] - yrange[0]) - yrange[0] /
                 (yrange[1] - yrange[0])));
        assert(0 <= yymin && yymin <= yymax &&
                yymax <= unsigned(m_dim[1] - 5));
        assert(yymin <= yymax);
        // join up with the previously drawn point
        if (yymaxlast + 1 < yymin) yymin = yymaxlast + 1;
        if (0 < yyminlast && yyminlast - 1 > yymax) yymax = yyminlast - 1;
        assert(yymin <= yymax);
        // draw
        for (unsigned yy = yymin; yy <= yymax; ++yy) {
            assert(0 <= xx && xx <= unsigned(m_dim[0] - 6));
            assert(0 <= yy && yy <= unsigned(m_dim[1] - 5));
            m_screen[m_dim[1] - 3 - yy][xx + 6] = marker;
        }
        yyminlast = yymin, yymaxlast = yymax;
    }
    std::size_t entries;
    double mean, rms, uflo, oflo;
    std::tie(entries, mean, rms, uflo, oflo) = stats();
    drawStats(entries, mean, rms, uflo, oflo);
}

void TextCanvas::normaliseScreen()
{
    for (auto& line: m_screen) {
        // remove trailing spaces, for the benefit of slower terminals
        auto rit = line.rbegin();
        for (; line.rend() != rit && std::isspace(*rit); ++rit);
        if (line.rend() == rit) continue;
        line.erase(rit.base(), line.end());
    }
}

void TextCanvas::drawScreen()
{
    for (const auto& line: m_screen) {
        m_os << line << std::endl;
    }
}

// vim: sw=4:tw=78:et:ft=cpp
